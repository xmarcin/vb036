\documentclass[aspectratio=169]{beamer}


\usepackage[british]{babel}
\usepackage[T1]{fontenc}
\usepackage{fontspec}
\usepackage{csquotes}
\usepackage{helvet}
\usepackage{hyperref}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{arrows}
\usepackage[backend=biber,style=ieee]{biblatex}
\addbibresource{sources.bib}

\usetheme[]{FI}

\author{Juraj Marcin}
\renewcommand{\authormail}{xmarcin@fi.muni.cz}
\title{Git}
\institute{Faculty of Informatics, Masaryk University}
\date{6th April 2020}

\begin{document}

\maketitleframe{}

\begin{frame}
    \frametitle{Outline}
    \framesubtitle{\insertsubsection}

    \tableofcontents

\end{frame}

\section{What is Git}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}
            \node (cloud) {
                \includegraphics[width=0.3\textwidth]{cloud.png}
            };
            \onslide<2-> {
                \draw[red, line width=1mm]
                    (cloud.south west) -- (cloud.north east)
                    (cloud.south east) -- (cloud.north west);
            }
            \onslide<3-> {
                \node (snapshots) [right=of cloud] {
                    \includegraphics[width=0.5\textwidth]{snapshots.png}
                };
            }
        \end{tikzpicture}

        \caption{\cite{cloud},~\cite{snapshots}}
    \end{figure}

\end{frame}

\subsection{What can you use it for}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{itemize}
        \item coding projects
        \item school notes
        \item configuration files
        \item thesis in \LaTeX{}
        \item \ldots
    \end{itemize}
    \vspace{2cm}

\end{frame}

\section{Basic Usage of Git}

\subsection{Managing local changes}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}[
            roundrectnode/.style={rectangle, draw=black, inner sep=.2cm,
                very thick,rounded corners=.2cm},
            untrackednode/.style={draw=color@gray7, fill=color@gray1},
            trackednode/.style={draw=color@phil, fill=color@phil!10},
            modifiednode/.style={draw=color@med, fill=color@med!10},
            stagednode/.style={draw=color@orange1, fill=color@orange1!10},
            commitednode/.style={draw=color@sci, fill=color@sci!10},
            arrow/.style={thick}
        ]
            \node[roundrectnode] (file) {File};
            \node[roundrectnode,untrackednode] (untracked) [left=of file]
                {Untracked};
            \node[roundrectnode,trackednode] (tracked) [right=of file]
                {Tracked};

            \node[roundrectnode,stagednode] (staged) [right=of tracked]
                {Staged};
            \node[roundrectnode,modifiednode] (modified) [above=of staged]
                {Modified};
            \node[roundrectnode,commitednode] (committed) [below=of staged]
                {Committed};

            \draw[->,arrow] (file.west) -- (untracked.east);
            \draw[->,arrow] (file.east) -- (tracked.west);
            \draw[->,arrow] (tracked.east) -- (modified.west);
            \draw[->,arrow] (tracked.east) -- (staged.west);
            \draw[->,arrow] (tracked.east) -- (committed.west);

            \onslide<2-> {
                \draw[->,arrow] (modified.south) -- (staged.north)
                    node[midway,right] {git add <file>};
            }
            \onslide<3-> {
                \draw[->,arrow] (staged.south) -- (committed.north)
                    node[midway,right] {git commit};
            }
        \end{tikzpicture}

        \caption{File states}
    \end{figure}

\end{frame}

\subsection{Syncing your work}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}[
            squarenode/.style={rectangle, inner sep=.2cm, very thick,
                rounded corners=.2cm, minimum size=1cm},
            arrow/.style={thick}
        ]
            \node (center) {};

            \node[squarenode, draw=color@fsps, fill=color@fsps!10] (local)
                [left=of center] {Local directory};
            \node[squarenode, draw=color@econ, fill=color@econ!10] (remote)
                [right=of center] {Remote server};
            \onslide<2-> {
                \draw[->,arrow] (remote.north) .. controls (0, 2)
                    .. (local.north) node[midway,above] {git pull};
            }
            \onslide<3-> {
                \draw[->,arrow] (local.south) .. controls (0, -2)
                    .. (remote.south) node[midway,below] {git push};
            }
        \end{tikzpicture}

        \caption{Remote sync}
    \end{figure}

\end{frame}

\section{Branching}

\subsection{Branches}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}[
            mastercommit/.style={circle, draw=color@muni, fill=color@muni!10,
                very thick, minimum size=.5cm},
            mergecommit/.style={circle, draw=color@fi, fill=color@fi!10,
                very thick, minimum size=.5cm},
            devcommit/.style={circle, draw=color@med, fill=color@med!10,
                very thick, minimum size=.5cm},
            testcommit/.style={circle, draw=color@phil, fill=color@phil!10,
                very thick, minimum size=.5cm},
            masterbranch/.style={-, thick, draw=color@muni},
            devbranch/.style={-, thick, draw=color@med},
            testbranch/.style={-, thick, draw=color@phil}
        ]
            \node (master) {master};

            \node[mastercommit] (initial) [above=.2cm of master] {};
            \node[mastercommit] (commit1) [above=.2cm of initial] {};
            \node[mastercommit] (commit2) [above=.2cm of commit1] {};
            \draw[masterbranch] (initial) -- (commit1);
            \draw[masterbranch] (commit1) -- (commit2);

            \node[devcommit] (devcommit1) [right=1cm of commit1] {};
            \node[devcommit] (devcommit2) [above=.2cm of devcommit1] {};
            \draw[devbranch] (initial) -- (devcommit1);
            \draw[devbranch] (devcommit1) -- (devcommit2);
            \node (dev) [below=.4cm of devcommit1] {dev};

            \node[testcommit] (testcommit1) [left=1cm of commit2] {};
            \node[testcommit] (testcommit2) [above=.2cm of testcommit1] {};
            \draw[testbranch] (commit1) -- (testcommit1);
            \draw[testbranch] (testcommit1) -- (testcommit2);
            \node (test) [below=.4cm of testcommit1] {test};

            \onslide<2-> {
                \node[mergecommit] (commit3) [above=.2cm of commit2] {};
                \draw[masterbranch] (commit2) -- (commit3);
                \draw[devbranch] (devcommit2) -- (commit3.south);

                \node[mergecommit] (commit4) [above=.2cm of commit3] {};
                \draw[masterbranch] (commit3) -- (commit4);
                \draw[testbranch] (testcommit2) -- (commit4.south);
            }
        \end{tikzpicture}

        \caption{Branch tree}
    \end{figure}
\end{frame}

\subsection{Use of Branches}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}[
            mastercommit/.style={circle, draw=color@muni, fill=color@muni!10,
                very thick, minimum size=.5cm},
            featureacommit/.style={circle, draw=color@med, fill=color@med!10,
                very thick, minimum size=.5cm},
            bugfixbcommit/.style={circle, draw=color@phil, fill=color@phil!10,
                very thick, minimum size=.5cm},
            featureccommit/.style={circle, draw=color@sci, fill=color@sci!10,
                very thick, minimum size=.5cm},
            featuredcommit/.style={circle, draw=color@law, fill=color@law!10,
                very thick, minimum size=.5cm},
            masterbranch/.style={-, thick, draw=color@muni},
            featureabranch/.style={-, thick, draw=color@med},
            bugfixbbranch/.style={-, thick, draw=color@phil},
            featurecbranch/.style={-, thick, draw=color@sci},
            featuredbranch/.style={-, thick, draw=color@law}
        ]
            \node (master) {master};
            \node[mastercommit] (commit1) [right=.75cm of master] {};
            \node[mastercommit] (commit2) [right=.5cm of commit1] {};
            \onslide<1-1> {
                \node[mastercommit] (commit3) [right=.5cm of commit2] {};
                \node[mastercommit] (commit4) [right=.5cm of commit3] {};
            }
            \node[mastercommit] (commit5) [right=.5cm of commit4] {};
            \node[mastercommit] (commit6) [right=.5cm of commit5] {};
            \onslide<1-1> {
                \node[mastercommit] (commit7) [right=.5cm of commit6] {};
                \node[mastercommit] (commit8) [right=.5cm of commit7] {};
            }
            \node[mastercommit] (commit9) [right=.5cm of commit8] {};
            \node[mastercommit] (commit10) [right=.5cm of commit9] {};`'
            \node[mastercommit] (commit11) [right=.5cm of commit10] {};
            \draw[masterbranch] (commit1) -- (commit2);
            \draw[masterbranch] (commit2) -- (commit3);
            \draw[masterbranch] (commit3) -- (commit4);
            \draw[masterbranch] (commit4) -- (commit5);
            \draw[masterbranch] (commit5) -- (commit6);
            \draw[masterbranch] (commit6) -- (commit7);
            \draw[masterbranch] (commit7) -- (commit8);
            \draw[masterbranch] (commit8) -- (commit9);
            \draw[masterbranch] (commit9) -- (commit10);
            \draw[masterbranch] (commit10) -- (commit11);

            \onslide<2-> {
                \draw[masterbranch] (commit2) -- (commit5);
                \draw[masterbranch] (commit6) -- (commit9);

                \node (featurea) [above=.5cm of master] {feature a};
                \node[featureacommit] (commit1a) [above=.5cm of commit2] {};
                \node[featureacommit] (commit2a) [right=.5cm of commit1a] {};
                \node[featureacommit] (commit3a) [right=.5cm of commit2a] {};
                \node[featureacommit] (commit4a) [right=.5cm of commit3a] {};
                \draw[featureabranch] (commit1) -- (commit1a);
                \draw[featureabranch] (commit1a) -- (commit2a);
                \draw[featureabranch] (commit2a) -- (commit3a);
                \draw[featureabranch] (commit3a) -- (commit4a);
                \draw[featureabranch] (commit4a) -- (commit6.west);

                \node (bugfixb) [below=.5cm of master] {bugfix b};
                \node[bugfixbcommit] (commit1b) [below=1.5cm of commit2a] {};
                \node[bugfixbcommit] (commit2b) [right=.5cm of commit1b] {};
                \draw[bugfixbbranch] (commit2) -- (commit1b);
                \draw[bugfixbbranch] (commit1b) -- (commit2b);
                \draw[bugfixbbranch] (commit2b) -- (commit5.west);

                \node (featurec) [above=.5cm of featurea] {feature c};
                \node[featureccommit] (commit1c) [above=1.5cm of commit6] {};
                \node[featureccommit] (commit2c) [right=.5cm of commit1c] {};
                \node[featureccommit] (commit3c) [right=.5cm of commit2c] {};
                \node[featureccommit] (commit4c) [right=.5cm of commit3c] {};
                \node[featureccommit] (commit5c) [right=.5cm of commit4c] {};
                \draw[featurecbranch] (commit5) -- (commit1c);
                \draw[featurecbranch] (commit1c) -- (commit2c);
                \draw[featurecbranch] (commit2c) -- (commit3c);
                \draw[featurecbranch] (commit3c) -- (commit4c);
                \draw[featurecbranch] (commit4c) -- (commit5c);
                \draw[featurecbranch] (commit5c) -- (commit11.west);

                \node (featured) [below=.5cm of bugfixb] {feature d};
                \node[featuredcommit] (commit1d) [below=1.5cm of commit6] {};
                \node[featuredcommit] (commit2d) [right=.5cm of commit1d] {};
                \node[featuredcommit] (commit3d) [right=.5cm of commit2d] {};
                \draw[featuredbranch] (commit5) -- (commit1d);
                \draw[featuredbranch] (commit1d) -- (commit2d);
                \draw[featuredbranch] (commit2d) -- (commit3d);
                \draw[featuredbranch] (commit3d) -- (commit9.west);
            }

        \end{tikzpicture}

        \caption{Branches}
    \end{figure}

\end{frame}

\subsection{Working with Branches}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{figure}
        \begin{tikzpicture}[
            blankcommit/.style={circle, very thick, minimum size=0.5cm},
            mastercommit/.style={circle, draw=color@muni, fill=color@muni!10,
                very thick, minimum size=.5cm},
            featurecommit/.style={circle, draw=color@med, fill=color@med!10,
                very thick, minimum size=.5cm},
            masterbranch/.style={-, thick, draw=color@muni},
            featurebranch/.style={-, thick, draw=color@med}
        ]
            \node (master) {master};
            \node[mastercommit] (commit1) [right=.75cm of master] {};
            \node[mastercommit] (commit2) [right=of commit1] {};
            \node[mastercommit] (commit3) [right=of commit2] {};
            \node[blankcommit] (blankc4) [right=of commit3] {};
            \node[blankcommit] (blankc5) [right=of blankc4] {};
            \draw[masterbranch] (commit1) -- (commit2);
            \draw[masterbranch] (commit2) -- (commit3);

            \onslide<2-> {
                \node (feature) [below=of master] {feature};
                \node[featurecommit] (commit4) [below=of blankc4] {};
                \draw[featurebranch] (commit3) -- (commit4)
                    node[midway,left] {git checkout -b feature};
            }
            \onslide<3-> {
                \node[featurecommit] (commit5) [right=of commit4] {};
                \draw[featurebranch] (commit4) -- (commit5);
            }
            \onslide<4-> {
                \node[featurecommit] (commit6) [right=of commit5] {};
                \draw[featurebranch] (commit5) -- (commit6);
            }

            \onslide<5-> {
                \node[mastercommit] (mastercommits) [right=of blankc5] {...};
                \draw[masterbranch] (commit3) -- (mastercommits)
                    node[midway,above] {git checkout master};
            }
            \onslide<6-> {
                \node[mastercommit] (commit7) [right=of mastercommits] {};
                \draw[masterbranch] (mastercommits) -- (commit7);
                \draw[featurebranch] (commit6) -- (commit7)
                    node[midway,right] {git merge feature};
            }
        \end{tikzpicture}

        \caption{Example project structure}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{\insertsection}
    \framesubtitle{\insertsubsection}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{github_merge.png}
                \caption{GitHub Pull Request UI~\cite{githubMerge}}
            \end{figure}
        \end{column}

        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{gitlab_merge.png}
                \caption{GitLab Merge Request UI~\cite{gitlabMerge}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{Conclusion}

    \tableofcontents

\end{frame}

\begin{frame}
    \frametitle{Further Reading}

    \begin{itemize}
        \item Pro Git book by Scott Chacon and Ben Straub
            \url{https://git-scm.com/book/en/v2}
        \item Git Handbook by GitHub
            \url{https://guides.github.com/introduction/git-handbook/}
    \end{itemize}
    \vspace{2cm}

\end{frame}

\begin{frame}
    \frametitle{Sources}

    \printbibliography{}

\end{frame}

\end{document}
